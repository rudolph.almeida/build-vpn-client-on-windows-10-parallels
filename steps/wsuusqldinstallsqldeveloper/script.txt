$ZIP="c:\software\sqldeveloper-18.4.0-376.1900-x64.zip"
$DEST="c:\Program Files\sqldeveloper"
$EXTRACT_DEST="c:\Program Files"
$EXE="$DEST\sqldeveloper.exe"
$SEND_TO_TOYS_DIR="c:\Program Files\Send To Toys"

function Make-Shortcut ([string] $DestPath) {
    $DestLink = "$DestPath\sqldeveloper.lnk"
    if (Test-Path "$DestLink") { Remove-Item "$DestLink" }
    
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut("$DestLink")
    $Shortcut.TargetPath = "$EXE"
    $Shortcut.Save()
}

if (Test-Path "$DEST") {
    "Removing old dir $DEST"
    Remove-Item "$DEST" -force -recurse
}

"Extracting $ZIP -> $DEST"
Expand-Archive -LiteralPath "$ZIP" -DestinationPath "$EXTRACT_DEST"

"Creating start menu shortcut"
Make-Shortcut "C:\ProgramData\Microsoft\Windows\Start Menu"
Make-Shortcut "C:\Users\Public\Desktop"
